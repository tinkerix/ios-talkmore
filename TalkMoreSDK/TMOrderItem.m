#import <Mantle/Mantle.h>
#import "TMOrderItem.h"

@implementation TMOrderItem

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return [NSDictionary mtl_identityPropertyMapWithModel:self];
}

+ (NSValueTransformer *)rechargedByJSONTransformer {
    return [MTLValueTransformer transformerUsingForwardBlock:^id(NSString *rechargedBy, BOOL *success, NSError **error) {
        return @(TMReChargeTypeFromNSString(rechargedBy));
    } reverseBlock:^id(NSNumber *rechargedBy, BOOL *success, NSError **error) {
        return NSStringFromTMReChargeType([rechargedBy unsignedIntegerValue]);
    }];
}

+ (NSValueTransformer *)statusJSONTransformer {
    return [MTLValueTransformer transformerUsingForwardBlock:^id(NSString *status, BOOL *success, NSError **error) {
        return @(TMOrderItemStatusFromNSString(status));
    } reverseBlock:^id(NSNumber *status, BOOL *success, NSError **error) {
        return NSStringFromTMOrderItemStatus([status unsignedIntegerValue]);
    }];
}

+ (NSValueTransformer *)createdOnJSONTransformer {
    return [MTLValueTransformer transformerUsingForwardBlock:^id(NSString *dateString, BOOL *success, NSError **error) {
        return [NSDate dateWithTimeIntervalSince1970:[dateString longLongValue]];
    } reverseBlock:^id(NSDate *date, BOOL *success, NSError **error) {
        return [[NSNumber numberWithDouble:[date timeIntervalSince1970]] stringValue];
    }];
}

+ (NSValueTransformer *)updatedOnJSONTransformer {
    return [MTLValueTransformer transformerUsingForwardBlock:^id(NSString *dateString, BOOL *success, NSError **error) {
        return [NSDate dateWithTimeIntervalSince1970:[dateString longLongValue]];
    } reverseBlock:^id(NSDate *date, BOOL *success, NSError **error) {
        return [[NSNumber numberWithDouble:[date timeIntervalSince1970]] stringValue];
    }];
}

@end
