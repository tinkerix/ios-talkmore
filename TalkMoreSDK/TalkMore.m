#import <Mantle/Mantle.h>
#import "TalkMore.h"
#import "TMPlanInfo.h"
#import "TMPlans.h"

NSString *const DefaultBaseURLPath = @"https://api.talkmoreapp.com";
NSString *const DefaultBrowseURLPath = @"/v2/browse";
NSString *const DefaultRechargeURLPath = @"/v2/recharge/oneshot";
NSString *const DefaultGetOrderURLPath = @"/v2/recharge/";
NSString *const DefaultGetPartnerOrderURLPath = @"/v2/recharge/oneshot/";
NSString *const BearerConstant = @"Bearer ";


@implementation TalkMore : NSObject

- (instancetype)initWithApiKey:(NSString *)apiKey {
    self = [super init];
    
    if (self) {
        _apiKey = apiKey;
        _baseURLPath = DefaultBaseURLPath;

        // Create the session configuration.
        NSURLSessionConfiguration *defaultSessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    
        // Setup the bearer token HTTP header.
        NSString *bearerToken = [BearerConstant stringByAppendingString:apiKey];
        [defaultSessionConfig setHTTPAdditionalHeaders:@{@"Authorization" : bearerToken}];
    
        // Build the session.
        _session = [NSURLSession sessionWithConfiguration:defaultSessionConfig];
    }

    return self;
}

- (instancetype)initWithApiKeyAndBaseURLPath:(NSString *)apiKey withBaseURLPath:(NSString *)baseURLPath {
    self = [self initWithApiKey:apiKey];
    if (self) {
        _baseURLPath = baseURLPath;
    }
    return self;
}

- (void)browsePlans:(TMProviderType)providerType forMobileServiceType:(TMMobileServiceType)mobileServiceType forCircle:(TMCircle)circle completion:(void (^)(TMPlans *plans, NSError *error))completion {
    // Setup the URL path.
    NSString *path = [self.baseURLPath stringByAppendingString:DefaultBrowseURLPath];
    
    // Setup the URL Parameters.
    NSURLComponents *components = [[NSURLComponents alloc] initWithString:path];
    

    // Provider
    NSString *provider = NSStringFromTMProviderType(providerType);
    NSString *queryStr = [@"provider=" stringByAppendingString:[provider stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]];
    
    // Circle
    NSString *location = NSStringFromTMCircle(circle);
    queryStr = [queryStr stringByAppendingString:[@"&location=" stringByAppendingString:[location stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]]];
    
    // Mobile Service Type
    NSString *mobileService = NSStringFromTMMobileServiceType(mobileServiceType);
    queryStr = [queryStr stringByAppendingString:[@"&mobileservicetype=" stringByAppendingString:[mobileService stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]]];
    
    // Build the final URL.
    components.query = queryStr;
    NSURL *browseUrl = components.URL;
    
    // Create task.
    NSURLSessionDataTask *browseTask = [self.session dataTaskWithURL:browseUrl completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (response != nil && error == nil) {
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
            if (httpResponse.statusCode != 200) {
                error = [[NSError alloc] initWithDomain:@"Error" code:httpResponse.statusCode userInfo:nil];
            }
        }
        if (error == nil) {
            // Lets parse it first into a dictionary.
            NSError *jsonError = nil;
            NSDictionary *rawPlanDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError];
            if (!jsonError) {
                TMPlans *plans = [[TMPlans alloc] init];
                for (id key in rawPlanDictionary) {
                    TMPlanTag planTag = TMPlanTagFromNSString((NSString *) key);
                    NSArray *rawPlanInfos = (NSArray *) rawPlanDictionary[key];
                    NSMutableArray *planInfos = [NSMutableArray arrayWithCapacity:[rawPlanInfos count]];
                    for (int i = 0; i < [rawPlanInfos count]; i++) {
                        NSDictionary *rawPlanInfo = (NSDictionary *) rawPlanInfos[i];
                        TMPlanInfo *planInfo = [MTLJSONAdapter modelOfClass:TMPlanInfo.class fromJSONDictionary:rawPlanInfo error:&jsonError];
                        if (!jsonError) {
                            planInfos[i] = planInfo;
                        } else {
                            break;
                        }
                    }
                    if (!jsonError) {
                        [plans addTagAndPlans:planTag withPlans:[planInfos copy]];
                    } else {
                        break;
                    }
                }
                if (!jsonError) {
                    completion(plans, nil);
                } else {
                    completion(nil, jsonError);
                }
            } else {
                completion(nil, jsonError);
            }
        } else {
            completion(nil, error);
        }
    }];
    [browseTask resume];
}

- (void)doRecharge:(NSString *)orderId forMobile:(NSString *)mobile forEmail:(NSString *)email forProviderType:(TMProviderType)providerType forMobileServiceType:(TMMobileServiceType)mobileServiceType forCircle:(TMCircle)circle forPostPaid:(BOOL)postPaid forAmount:(int)amount forRecharges:(NSString *)recharges forDeviceId:(NSString *)deviceId forInstallId:(NSString *)installId forSpecialRecharge:(BOOL)specialRecharge completion:(void (^)(TMOrder *order, NSError *error))completion {
    // Setup the URL path.
    NSString *path = [self.baseURLPath stringByAppendingString:DefaultRechargeURLPath];

    // Create request
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:path]];
    
    // HTTP POST
    [request setHTTPMethod:@"POST"];

    // Content Type for the form
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];

    // HTTP POST Params
    NSString *postPaidParam;
    if (postPaid) {
        postPaidParam = @"true";
    } else {
        postPaidParam = @"false";
    }
    
    NSString *specialRechargeParam;
    if (specialRecharge) {
        specialRechargeParam = @"true";
    } else {
        specialRechargeParam = @"false";
    }
    
    NSMutableDictionary *rechargeParams = [NSMutableDictionary
                                           dictionaryWithDictionary:@{@"orderid" : orderId,
                                                                      @"provider" : NSStringFromTMProviderType(providerType),
                                                                      @"location" : NSStringFromTMCircle(circle),
                                                                      @"mobileservicetype" : NSStringFromTMMobileServiceType(mobileServiceType),
                                                                      @"mobile" : mobile,
                                                                      @"email" : email,
                                                                      @"postpaid": postPaidParam,
                                                                      @"specialrecharge": specialRechargeParam,
                                                                      @"amount" : [@(amount) stringValue],
                                                                      @"recharges" : recharges}];
    rechargeParams[@"deviceid"] = deviceId ? deviceId : [NSNull null];
    rechargeParams[@"installid"] = installId ? installId : [NSNull null];
    NSMutableArray *paramPairs = [NSMutableArray array];
    for (id key in rechargeParams) {
        NSString *name = (NSString *)key;
        NSString *value;
        id rawValue = rechargeParams[key];
        if (rawValue == [NSNull null]) {
            value = @"";
        } else {
            value = [(NSString *)rawValue stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        }
        [paramPairs addObject:[name stringByAppendingFormat:@"=%@", value]];
    }
    NSString * query = [paramPairs componentsJoinedByString:@"&"];

        // Convert params into http body
    [request setHTTPBody:[query dataUsingEncoding:NSUTF8StringEncoding]];
    
    // Create task.
    NSURLSessionDataTask *rechargeTask = [self.session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (response != nil && error == nil) {
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
            if (httpResponse.statusCode != 200) {
                error = [[NSError alloc] initWithDomain:@"Error" code:httpResponse.statusCode userInfo:nil];
            }
        }
        if (error == nil) {
            // Lets parse it first into a dictionary.
            NSError *jsonError = nil;
            NSDictionary *rawOrder = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError];
            if (!jsonError) {
                TMOrder *order = [MTLJSONAdapter modelOfClass:TMOrder.class fromJSONDictionary:rawOrder error:&jsonError];
                if (!jsonError) {
                    completion(order, nil);
                } else {
                    completion(nil, jsonError);
                }
            } else {
                completion(nil, jsonError);
            }
        } else {
            completion(nil, error);
        }
    }];
    [rechargeTask resume];
}

- (void)getOrder:(NSString *)orderId completion:(void (^)(TMOrder *order, NSError *error))completion {
    // Setup the URL path.
    NSString *path = [[self.baseURLPath stringByAppendingString:DefaultGetOrderURLPath] stringByAppendingString:orderId];
    
    // Setup the URL Parameters.
    NSURLComponents *components = [[NSURLComponents alloc] initWithString:path];
    
    NSURL *getOrderUrl = components.URL;
    
    // Create task.
    NSURLSessionDataTask *getOrderTask = [self.session dataTaskWithURL:getOrderUrl completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (response != nil && error == nil) {
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
            if (httpResponse.statusCode != 200) {
                error = [[NSError alloc] initWithDomain:@"Error" code:httpResponse.statusCode userInfo:nil];
            }
        }
        if (error == nil) {
            // Lets parse it first into a dictionary.
            NSError *jsonError = nil;
            NSDictionary *rawOrder = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError];
            if (!jsonError) {
                TMOrder *order = [MTLJSONAdapter modelOfClass:TMOrder.class fromJSONDictionary:rawOrder error:&jsonError];
                if (!jsonError) {
                    completion(order, nil);
                } else {
                    completion(nil, jsonError);
                }
            } else {
                completion(nil, jsonError);
            }
        } else {
            completion(nil, error);
        }
    }];
    [getOrderTask resume];
}

- (void)getOrderByPartnerId:(NSString *)orderId completion:(void (^)(TMOrder *order, NSError *error))completion {
    // Setup the URL path.
    NSString *path = [[self.baseURLPath stringByAppendingString:DefaultGetPartnerOrderURLPath] stringByAppendingString:orderId];
    
    // Setup the URL Parameters.
    NSURLComponents *components = [[NSURLComponents alloc] initWithString:path];
    
    NSURL *getOrderUrl = components.URL;
    
    // Create task.
    NSURLSessionDataTask *getOrderTask = [self.session dataTaskWithURL:getOrderUrl completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (response != nil && error == nil) {
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
            if (httpResponse.statusCode != 200) {
                error = [[NSError alloc] initWithDomain:@"Error" code:httpResponse.statusCode userInfo:nil];
            }
        }
        if (error == nil) {
            // Lets parse it first into a dictionary.
            NSError *jsonError = nil;
            NSDictionary *rawOrder = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError];
            if (!jsonError) {
                TMOrder *order = [MTLJSONAdapter modelOfClass:TMOrder.class fromJSONDictionary:rawOrder error:&jsonError];
                if (!jsonError) {
                    completion(order, nil);
                } else {
                    completion(nil, jsonError);
                }
            } else {
                completion(nil, jsonError);
            }
        } else {
            completion(nil, error);
        }
    }];
    [getOrderTask resume];
}

@end
