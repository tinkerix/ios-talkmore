#ifndef TMOrder_h
#define TMOrder_h

#import <Mantle/Mantle.h>
#import "TMCommon.h"

@interface TMOrder : MTLModel <MTLJSONSerializing>

@property (nonatomic, copy, readonly) NSString *orderId;
@property (nonatomic, copy, readonly) NSString *mobile;
@property (nonatomic, copy, readonly) NSString *email;
@property (nonatomic, assign, readonly) TMProviderType provider;
@property (nonatomic, assign, readonly) TMMobileServiceType mobileServiceType;
@property (nonatomic, assign, readonly) TMCircle location;
@property (nonatomic, assign, readonly) TMPaymentType paidBy;
@property (nonatomic, copy, readonly) NSString *paidId;
@property (nonatomic, assign, readonly) int amount;
@property (nonatomic, copy, readonly) NSString *recharges;
@property (nonatomic, copy, readonly) NSString *extra;
@property (nonatomic, assign, readonly) TMOrderStatus status;
@property (nonatomic, copy, readonly) NSDate *createdOn;
@property (nonatomic, copy, readonly) NSDate *updatedOn;
@property (nonatomic, copy, readonly) NSArray *orderItems;
@property (nonatomic, copy, readonly) NSString *orderCanceledReason;
@property (nonatomic, assign, readonly) BOOL verified;
@property (nonatomic, copy, readonly) NSString *coupon;

@end

#endif /* TMOrder_h */
