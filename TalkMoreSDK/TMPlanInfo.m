#import "TMCommon.h"
#import "TMPlanInfo.h"

@implementation TMPlanInfo

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return [NSDictionary mtl_identityPropertyMapWithModel:self];
}

+ (NSValueTransformer *)typeJSONTransformer {
    return [MTLValueTransformer transformerUsingForwardBlock:^id(NSString *planType, BOOL *success, NSError **error) {
        return @(TMPlanTypeFromNSString(planType));
    } reverseBlock:^id(NSNumber *planType, BOOL *success, NSError **error) {
        return NSStringFromTMPlanType([planType unsignedIntegerValue]);
    }];
}

@end
