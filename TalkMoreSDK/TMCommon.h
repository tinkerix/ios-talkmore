#ifndef TMCommon_h
#define TMCommon_h

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, TMProviderType) {
    AIRCEL,
    AIRTEL,
    BSNL,
    IDEA,
    JIO,
    LOOP,
    MTNL,
    RELIANCE,
    STEL,
    SPICE,
    T24,
    TATA,
    TATA_INDICOM,
    UNINOR,
    VIDEOCON,
    VIRGIN,
    VODAFONE
};

typedef NS_ENUM(NSUInteger, TMMobileServiceType) {
    CDMA,
    GSM
};

typedef NS_ENUM(NSUInteger, TMCircle) {
    APH,
    ASM,
    BIH,
    CHE,
    DEL,
    GUJ,
    HAR,
    HIM,
    JNK,
    KAR,
    KER,
    KOL,
    MAH,
    MAP,
    MUM,
    NER,
    ORS,
    PUN,
    RAJ,
    TAM,
    UPE,
    UPW,
    WEB
};

typedef NS_ENUM(NSUInteger, TMPlanTag) {
    FULL_TALKTIME,
    FULL_TALKTIME_PLUS,
    TOP_UP,
    LOCAL,
    STD,
    ISD,
    SMS,
    DATA_2G,
    DATA_3G,
    DATA_4G,
    ROAMING,
    FIRST_RECHARGE,
    PLAN_VOUCHER,
    NIGHT
};

typedef NS_ENUM(NSUInteger, TMPlanType) {
    PLAN_BASIC,
    PLAN_RATE_CUTTER,
    PLAN_FULL_TALKTIME,
    PLAN_TOP_UP
};

typedef NS_ENUM(NSUInteger, TMPaymentType) {
    PAYTM,
    PAYTM_STAGING,
    PARTNER
};

typedef NS_ENUM(NSUInteger, TMOrderStatus) {
    ORDER_CREATED,
    ORDER_PAID_HINT,
    ORDER_PAID,
    ORDER_TRANSACTION_SUCCESS,
    ORDER_IN_PROGRESS,
    ORDER_DONE_SUCCESS,
    ORDER_DONE_PARTIAL,
    ORDER_DONE_FAILED,
    ORDER_DONE_FAILED_UNPAID,
    ORDER_CANCELED
};

typedef NS_ENUM(NSUInteger, TMReChargeType) {
    JRI,
    SKIP
};

typedef NS_ENUM(NSUInteger, TMOrderItemStatus) {
    ITEM_CREATED,
    ITEM_IN_PROGRESS,
    ITEM_DONE_SUCCESS,
    ITEM_DONE_FAILED,
    ITEM_DONE_TIMEOUT
};

extern NSString * NSStringFromTMProviderType(TMProviderType providerType);

extern TMProviderType TMProviderTypeFromNSString(NSString *providerType);

extern NSString * NSStringFromTMMobileServiceType(TMMobileServiceType mobileServiceType);

extern TMMobileServiceType TMMobileServiceTypeFromNSString(NSString *mobileServiceType);

extern NSString * NSStringFromTMCircle(TMCircle circle);

extern TMCircle TMCircleFromNSString(NSString *circle);

extern NSString * NSStringFromTMPlanTag(TMPlanTag planTag);

extern TMPlanTag TMPlanTagFromNSString(NSString *planTag);

extern NSString * NSStringFromTMPlanType(TMPlanType planType);

extern TMPlanType TMPlanTypeFromNSString(NSString *planType);

extern NSString * NSStringFromTMPaymentType(TMPaymentType paymentType);

extern TMPaymentType TMPaymentTypeFromNSString(NSString *paymentType);

extern NSString * NSStringFromTMOrderStatus(TMOrderStatus orderStatus);

extern TMOrderStatus TMOrderStatusFromNSString(NSString *orderStatus);

extern NSString * NSStringFromTMReChargeType(TMReChargeType reChargeType);

extern TMReChargeType TMReChargeTypeFromNSString(NSString *reChargeType);

extern NSString * NSStringFromTMOrderItemStatus(TMOrderItemStatus orderItemStatus);

extern TMOrderItemStatus TMOrderItemStatusFromNSString(NSString *orderItemStatus);

#endif /* TMCommon_h */
