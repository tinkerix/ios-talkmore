#import <Mantle/Mantle.h>
#import "TMOrder.h"
#import "TMOrderItem.h"

@implementation TMOrder

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return [NSDictionary mtl_identityPropertyMapWithModel:self];
}

+ (NSValueTransformer *)providerJSONTransformer {
    return [MTLValueTransformer transformerUsingForwardBlock:^id(NSString *provider, BOOL *success, NSError **error) {
        return @(TMProviderTypeFromNSString(provider));
    } reverseBlock:^id(NSNumber *provider, BOOL *success, NSError **error) {
        return NSStringFromTMProviderType([provider unsignedIntegerValue]);
    }];
}

+ (NSValueTransformer *)mobileServiceTypeJSONTransformer {
    return [MTLValueTransformer transformerUsingForwardBlock:^id(NSString *mobileServiceType, BOOL *success, NSError **error) {
        return @(TMMobileServiceTypeFromNSString(mobileServiceType));
    } reverseBlock:^id(NSNumber *mobileServiceType, BOOL *success, NSError **error) {
        return NSStringFromTMMobileServiceType([mobileServiceType unsignedIntegerValue]);
    }];
}

+ (NSValueTransformer *)locationJSONTransformer {
    return [MTLValueTransformer transformerUsingForwardBlock:^id(NSString *circle, BOOL *success, NSError **error) {
        return @(TMCircleFromNSString(circle));
    } reverseBlock:^id(NSNumber *circle, BOOL *success, NSError **error) {
        return NSStringFromTMCircle([circle unsignedIntegerValue]);
    }];
}

+ (NSValueTransformer *)paidByJSONTransformer {
    return [MTLValueTransformer transformerUsingForwardBlock:^id(NSString *paidBy, BOOL *success, NSError **error) {
        return @(TMPaymentTypeFromNSString(paidBy));
    } reverseBlock:^id(NSNumber *paidBy, BOOL *success, NSError **error) {
        return NSStringFromTMPaymentType([paidBy unsignedIntegerValue]);
    }];
}

+ (NSValueTransformer *)statusJSONTransformer {
    return [MTLValueTransformer transformerUsingForwardBlock:^id(NSString *status, BOOL *success, NSError **error) {
        return @(TMOrderStatusFromNSString(status));
    } reverseBlock:^id(NSNumber *status, BOOL *success, NSError **error) {
        return NSStringFromTMOrderStatus([status unsignedIntegerValue]);
    }];
}

+ (NSValueTransformer *)orderItemsJSONTransformer {
    return [MTLJSONAdapter arrayTransformerWithModelClass:TMOrderItem.class];
}

+ (NSValueTransformer *)createdOnJSONTransformer {
    return [MTLValueTransformer transformerUsingForwardBlock:^id(NSString *dateString, BOOL *success, NSError **error) {
        return [NSDate dateWithTimeIntervalSince1970:[dateString longLongValue]];
    } reverseBlock:^id(NSDate *date, BOOL *success, NSError **error) {
        return [[NSNumber numberWithDouble:[date timeIntervalSince1970]] stringValue];
    }];
}

+ (NSValueTransformer *)updatedOnJSONTransformer {
    return [MTLValueTransformer transformerUsingForwardBlock:^id(NSString *dateString, BOOL *success, NSError **error) {
        return [NSDate dateWithTimeIntervalSince1970:[dateString longLongValue]];
    } reverseBlock:^id(NSDate *date, BOOL *success, NSError **error) {
        return [[NSNumber numberWithDouble:[date timeIntervalSince1970]] stringValue];
    }];
}

@end
