#ifndef TMPlans_h
#define TMPlans_h

#import "TMCommon.h"

@interface TMPlans : NSObject

@property (readonly, nonatomic) NSMutableDictionary *plans;

- (void)addTagAndPlans:(TMPlanTag)planTag withPlans:(NSArray*)planInfos;

- (BOOL)hasPlansForTag:(TMPlanTag)planTag;

- (NSArray *)getPlansForTag:(TMPlanTag)planTag;

@end

#endif /* TMPlans_h */
