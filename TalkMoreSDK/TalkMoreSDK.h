//
//  TalkMoreSDK.h
//  TalkMoreSDK
//
//  Created by Chandrashekhar Shetty on 11/12/15.
//  Copyright © 2015 Tinkerix. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for TalkMoreSDK.
FOUNDATION_EXPORT double TalkMoreSDKVersionNumber;

//! Project version string for TalkMoreSDK.
FOUNDATION_EXPORT const unsigned char TalkMoreSDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <TalkMoreSDK/PublicHeader.h>

#import "TalkMore.h"
#import "TMCommon.h"
#import "TMPlans.h"
#import "TMPlanInfo.h"
#import "TMOrder.h"
#import "TMOrderItem.h"
