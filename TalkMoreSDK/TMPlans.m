#import "TMPlans.h"

@implementation TMPlans

- (instancetype)init {
    self = [super init];
    if (self) {
        _plans = [[NSMutableDictionary alloc] init];
    }
    return self;
}

- (void)addTagAndPlans:(TMPlanTag)planTag withPlans:(NSArray*)planInfos {
    [_plans setObject:planInfos forKey:@(planTag)];
}

- (BOOL)hasPlansForTag:(TMPlanTag)planTag {
    NSArray *planInfos = [_plans objectForKey:@(planTag)];
    if (planInfos) {
        return [planInfos count] > 0;
    } else {
        return FALSE;
    }
}

- (NSArray *)getPlansForTag:(TMPlanTag)planTag {
    NSArray *planInfos = [_plans objectForKey:@(planTag)];
    return planInfos;
}

@end