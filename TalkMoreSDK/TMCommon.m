#import "TMCommon.h"

NSString * NSStringFromTMProviderType(TMProviderType providerType) {
    switch (providerType) {
        case AIRCEL:
            return @"AIRCEL";
        case AIRTEL:
            return @"AIRTEL";
        case BSNL:
            return @"BSNL";
        case IDEA:
            return @"IDEA";
        case JIO:
            return @"JIO";
        case LOOP:
            return @"LOOP";
        case MTNL:
            return @"MTNL";
        case RELIANCE:
            return @"RELIANCE";
        case STEL:
            return @"STEL";
        case SPICE:
            return @"SPICE";
        case T24:
            return @"T24";
        case TATA:
            return @"TATA";
        case TATA_INDICOM:
            return @"TATA_INDICOM";
        case UNINOR:
            return @"UNINOR";
        case VIDEOCON:
            return @"VIDEOCON";
        case VIRGIN:
            return @"VIRGIN";
        case VODAFONE:
            return @"VODAFONE";
        default:
            return nil;
    }
}

TMProviderType TMProviderTypeFromNSString(NSString *providerType) {
    static NSDictionary *NSStringToTMProviderType = nil;
    if (!NSStringToTMProviderType) {
        NSStringToTMProviderType = @{@"AIRCEL" : @(AIRCEL),
                                     @"AIRTEL" : @(AIRTEL),
                                     @"BSNL" : @(BSNL),
                                     @"IDEA" : @(IDEA),
                                     @"JIO" : @(JIO),
                                     @"LOOP" : @(LOOP),
                                     @"MTNL" : @(MTNL),
                                     @"RELIANCE" : @(RELIANCE),
                                     @"STEL" : @(STEL),
                                     @"SPICE" : @(SPICE),
                                     @"T24" : @(T24),
                                     @"TATA" : @(TATA),
                                     @"TATA_INDICOM" : @(TATA_INDICOM),
                                     @"UNINOR" : @(UNINOR),
                                     @"VIDEOCON" : @(VIDEOCON),
                                     @"VIRGIN" : @(VIRGIN),
                                     @"VODAFONE" : @(VODAFONE)};
    }
    return [NSStringToTMProviderType[providerType] unsignedIntegerValue];
}

NSString * NSStringFromTMMobileServiceType(TMMobileServiceType mobileServiceType) {
    switch (mobileServiceType) {
        case GSM:
            return @"GSM";
        case CDMA:
            return @"CDMA";
        default:
            return nil;
    }
}

TMMobileServiceType TMMobileServiceTypeFromNSString(NSString *mobileServiceType) {
    static NSDictionary *NSStringToTMMobileServiceType = nil;
    if (!NSStringToTMMobileServiceType) {
        NSStringToTMMobileServiceType = @{@"GSM" : @(GSM), @"CDMA" : @(CDMA)};
    }
    return [NSStringToTMMobileServiceType[mobileServiceType] unsignedIntegerValue];
}

NSString * NSStringFromTMCircle(TMCircle circle) {
    switch (circle) {
        case APH:
            return @"APH";
        case ASM:
            return @"ASM";
        case BIH:
            return @"BIH";
        case CHE:
            return @"CHE";
        case DEL:
            return @"DEL";
        case GUJ:
            return @"GUJ";
        case HAR:
            return @"HAR";
        case HIM:
            return @"HIM";
        case JNK:
            return @"JNK";
        case KAR:
            return @"KAR";
        case KER:
            return @"KER";
        case KOL:
            return @"KOL";
        case MAH:
            return @"MAH";
        case MAP:
            return @"MAP";
        case MUM:
            return @"MUM";
        case NER:
            return @"NER";
        case ORS:
            return @"ORS";
        case PUN:
            return @"PUN";
        case RAJ:
            return @"RAJ";
        case TAM:
            return @"TAM";
        case UPE:
            return @"UPE";
        case UPW:
            return @"UPW";
        case WEB:
            return @"WEB";
        default:
            return nil;
    }
}

TMCircle TMCircleFromNSString(NSString *circle) {
    static NSDictionary *NSStringToTMCircle = nil;
    if (!NSStringToTMCircle) {
        NSStringToTMCircle = @{@"APH" : @(APH),
                               @"ASM" : @(ASM),
                               @"BIH" : @(BIH),
                               @"CHE" : @(CHE),
                               @"DEL" : @(DEL),
                               @"GUJ" : @(GUJ),
                               @"HAR" : @(HAR),
                               @"HIM" : @(HIM),
                               @"JNK" : @(JNK),
                               @"KAR" : @(KAR),
                               @"KER" : @(KER),
                               @"KOL" : @(KOL),
                               @"MAH" : @(MAH),
                               @"MAP" : @(MAP),
                               @"MUM" : @(MUM),
                               @"NER" : @(NER),
                               @"ORS" : @(ORS),
                               @"PUN" : @(PUN),
                               @"RAJ" : @(RAJ),
                               @"TAM" : @(TAM),
                               @"UPE" : @(UPE),
                               @"UPW" : @(UPW),
                               @"WEB" : @(WEB)};
    }
    return [NSStringToTMCircle[circle] unsignedIntegerValue];
}

NSString * NSStringFromTMPlanTag(TMPlanTag planTag) {
    switch (planTag) {
        case FULL_TALKTIME:
            return @"FULL_TALKTIME";
        case FULL_TALKTIME_PLUS:
            return @"FULL_TALKTIME_PLUS";
        case TOP_UP:
            return @"TOP_UP";
        case LOCAL:
            return @"LOCAL";
        case STD:
            return @"STD";
        case ISD:
            return @"ISD";
        case SMS:
            return @"SMS";
        case DATA_2G:
            return @"DATA_2G";
        case DATA_3G:
            return @"DATA_3G";
        case DATA_4G:
            return @"DATA_4G";
        case ROAMING:
            return @"ROAMING";
        case FIRST_RECHARGE:
            return @"FIRST_RECHARGE";
        case PLAN_VOUCHER:
            return @"PLAN_VOUCHER";
        case NIGHT:
            return @"NIGHT";
        default:
            return nil;
    }
}

TMPlanTag TMPlanTagFromNSString(NSString *planTag) {
    static NSDictionary *NSStringToTMPlanTag = nil;
    if (!NSStringToTMPlanTag) {
        NSStringToTMPlanTag = @{ @"FULL_TALKTIME" : @(FULL_TALKTIME),
                                 @"FULL_TALKTIME_PLUS" : @(FULL_TALKTIME_PLUS),
                                 @"TOP_UP" : @(TOP_UP),
                                 @"LOCAL" : @(LOCAL),
                                 @"STD" : @(STD),
                                 @"ISD" : @(ISD),
                                 @"SMS" : @(SMS),
                                 @"DATA_2G" : @(DATA_2G),
                                 @"DATA_3G" : @(DATA_3G),
                                 @"DATA_4G" : @(DATA_4G),
                                 @"ROAMING" : @(ROAMING),
                                 @"FIRST_RECHARGE" : @(FIRST_RECHARGE),
                                 @"PLAN_VOUCHER" : @(PLAN_VOUCHER),
                                 @"NIGHT" : @(NIGHT)};
    }
    return [NSStringToTMPlanTag[planTag] unsignedIntegerValue];
}

NSString *NSStringFromTMPlanType(TMPlanType planType) {
    switch (planType) {
        case PLAN_BASIC:
            return @"BASIC";
        case PLAN_RATE_CUTTER:
            return @"RATE_CUTTER";
        case PLAN_FULL_TALKTIME:
            return @"FULL_TALKTIME";
        case PLAN_TOP_UP:
            return @"TOP_UP";
        default:
            return nil;
    }
}

TMPlanType TMPlanTypeFromNSString(NSString *planType) {
    static NSDictionary *NSStringToTMPlanType = nil;
    if (!NSStringToTMPlanType) {
        NSStringToTMPlanType = @{@"BASIC" : @(PLAN_BASIC),
                                 @"RATE_CUTTER" : @(PLAN_RATE_CUTTER),
                                 @"FULL_TALKTIME" : @(PLAN_FULL_TALKTIME),
                                 @"TOP_UP" : @(PLAN_TOP_UP)};
    }
    return [NSStringToTMPlanType[planType] unsignedIntegerValue];
}

NSString *NSStringFromTMPaymentType(TMPaymentType paymentType) {
    switch (paymentType) {
        case PAYTM:
            return @"PAYTM";
        case PAYTM_STAGING:
            return @"PAYTM_STAGING";
        case PARTNER:
            return @"PARTNER";
        default:
            return nil;
    }
}

TMPaymentType TMPaymentTypeFromNSString(NSString *paymentType) {
    static NSDictionary *NSStringToTMPaymentType = nil;
    if (!NSStringToTMPaymentType) {
        NSStringToTMPaymentType = @{@"PAYTM" : @(PAYTM),
                                    @"PAYTM_STAGING" : @(PAYTM_STAGING),
                                    @"PARTNER" : @(PARTNER)};
    }
    return [NSStringToTMPaymentType[paymentType] unsignedIntegerValue];
}

NSString * NSStringFromTMOrderStatus(TMOrderStatus orderStatus) {
    switch (orderStatus) {
        case ORDER_CREATED:
            return @"CREATED";
        case ORDER_IN_PROGRESS:
            return @"IN_PROGRESS";
        case ORDER_PAID:
            return @"PAID";
        case ORDER_PAID_HINT:
            return @"PAID_HINT";
        case ORDER_CANCELED:
            return @"CANCELED";
        case ORDER_DONE_SUCCESS:
            return @"DONE_SUCCESS";
        case ORDER_DONE_FAILED:
            return @"DONE_FAILED";
        case ORDER_DONE_FAILED_UNPAID:
            return @"DONE_FAILED_UNPAID";
        case ORDER_DONE_PARTIAL:
            return @"DONE_PARTIAL";
        case ORDER_TRANSACTION_SUCCESS:
            return @"TRANSACTION_SUCCESS";
        default:
            return nil;
    }
}

TMOrderStatus TMOrderStatusFromNSString(NSString *orderStatus){
    static NSDictionary *NSStringToTMOrderStatus = nil;
    if (!NSStringToTMOrderStatus) {
        NSStringToTMOrderStatus = @{@"CREATED" : @(ORDER_CREATED),
                                    @"IN_PROGRESS" : @(ORDER_IN_PROGRESS),
                                    @"PAID_HINT" : @(ORDER_PAID_HINT),
                                    @"PAID" : @(ORDER_PAID),
                                    @"CANCELED" : @(ORDER_CANCELED),
                                    @"DONE_SUCCESS" : @(ORDER_DONE_SUCCESS),
                                    @"DONE_FAILED" : @(ORDER_DONE_FAILED),
                                    @"DONE_FAILED_UNPAID" : @(ORDER_DONE_FAILED_UNPAID),
                                    @"DONE_PARTIAL" : @(ORDER_DONE_PARTIAL),
                                    @"TRANSACTION_SUCCESS" : @(ORDER_TRANSACTION_SUCCESS)};
    }
    return [NSStringToTMOrderStatus[orderStatus] unsignedIntegerValue];
}

NSString * NSStringFromTMReChargeType(TMReChargeType reChargeType) {
    switch (reChargeType) {
        case JRI:
            return @"JRI";
        case SKIP:
            return @"SKIP";
        default:
            return nil;
    }
}

TMReChargeType TMReChargeTypeFromNSString(NSString *reChargeType) {
    static NSDictionary *NSStringToTMReChargeType = nil;
    if (!NSStringToTMReChargeType) {
        NSStringToTMReChargeType = @{@"JRI" : @(JRI), @"SKIP" : @(SKIP)};
    }
    return [NSStringToTMReChargeType[reChargeType] unsignedIntegerValue];
}

NSString * NSStringFromTMOrderItemStatus(TMOrderItemStatus orderItemStatus) {
    switch (orderItemStatus) {
        case ITEM_CREATED:
            return @"CREATED";
        case ITEM_IN_PROGRESS:
            return @"IN_PROGRESS";
        case ITEM_DONE_SUCCESS:
            return @"DONE_SUCCESS";
        case ITEM_DONE_FAILED:
            return @"DONE_FAILED";
        case ITEM_DONE_TIMEOUT:
            return @"DONE_TIMEOUT";
        default:
            return nil;
    }
}

TMOrderItemStatus TMOrderItemStatusFromNSString(NSString *orderItemStatus) {
    static NSDictionary *NSStringToTMOrderItemStatus = nil;
    if (!NSStringToTMOrderItemStatus) {
        NSStringToTMOrderItemStatus = @{@"CREATED" : @(ITEM_CREATED),
                                        @"IN_PROGRESS" : @(ITEM_IN_PROGRESS),
                                        @"DONE_SUCCESS" : @(ITEM_DONE_SUCCESS),
                                        @"DONE_FAILED" : @(ITEM_DONE_FAILED),
                                        @"DONE_TIMEOUT" : @(ITEM_DONE_TIMEOUT)};
    }
    return [NSStringToTMOrderItemStatus[orderItemStatus] unsignedIntegerValue];
}
