#ifndef TalkMore_h
#define TalkMore_h

#import "TMCommon.h"
#import "TMPlans.h"
#import "TMPlanInfo.h"
#import "TMOrder.h"
#import "TMOrderItem.h"

@interface TalkMore : NSObject

@property (readonly, nonatomic) NSString *baseURLPath;
@property (readonly, nonatomic) NSString *apiKey;
@property (readonly, nonatomic) NSURLSession *session;

- (instancetype)initWithApiKey:(NSString *)apiKey;

- (instancetype)initWithApiKeyAndBaseURLPath:(NSString *)apiKey withBaseURLPath:(NSString *)baseURLPath;

- (void)browsePlans:(TMProviderType)providerType forMobileServiceType:(TMMobileServiceType)mobileServiceType forCircle:(TMCircle)circle completion:(void (^)(TMPlans *plans, NSError *error))completion;

- (void)doRecharge:(NSString *)orderId forMobile:(NSString *)mobile forEmail:(NSString *)email forProviderType:(TMProviderType)providerType forMobileServiceType:(TMMobileServiceType)mobileServiceType forCircle:(TMCircle)circle forPostPaid:(BOOL)postPaid forAmount:(int)amount forRecharges:(NSString *)recharges forDeviceId:(NSString *)deviceId forInstallId:(NSString *)installId forSpecialRecharge:(BOOL)specialRecharge completion:(void (^)(TMOrder *order, NSError *error))completion;

- (void)getOrder:(NSString *)orderId completion:(void (^)(TMOrder *order, NSError *error))completion;

- (void)getOrderByPartnerId:(NSString *)orderId completion:(void (^)(TMOrder *order, NSError *error))completion;

@end

#endif /* TalkMore_h */
