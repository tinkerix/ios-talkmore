#ifndef TMOrderItem_h
#define TMOrderItem_h

#import <Mantle/Mantle.h>
#import "TMCommon.h"
#import "TMOrderItem.h"

@interface TMOrderItem : MTLModel <MTLJSONSerializing>

@property (nonatomic, readonly, assign) int id;
@property (nonatomic, readonly, copy) NSString * orderId;
@property (nonatomic, readonly, assign) int amount;
@property (nonatomic, readonly, assign) TMReChargeType rechargedBy;
@property (nonatomic, readonly, copy) NSString *rechargedId;
@property (nonatomic, readonly, copy) NSString *extra;
@property (nonatomic, readonly, assign) TMOrderItemStatus status;
@property (nonatomic, readonly, copy) NSDate *createdOn;
@property (nonatomic, readonly, copy) NSDate *updatedOn;
@property (nonatomic, readonly, copy) NSString *planType;
@property (nonatomic, readonly, copy) NSString *longDesc;
@property (nonatomic, readonly, assign) float talkTime;
@property (nonatomic, readonly, copy) NSString *validity;

@end

#endif /* TMOrderItem_h */
