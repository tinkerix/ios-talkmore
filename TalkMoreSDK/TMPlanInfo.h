#ifndef TMPlanInfo_h
#define TMPlanInfo_h

#import <Mantle/Mantle.h>
#import "TMCommon.h"

@interface TMPlanInfo : MTLModel <MTLJSONSerializing>

@property (nonatomic, readonly, assign) TMPlanType type;
@property (nonatomic, readonly, assign) int validity;
@property (nonatomic, readonly, copy) NSString *cost;
@property (nonatomic, readonly, copy) NSString *talkTime;
@property (nonatomic, readonly, copy) NSString *shortDesc;
@property (nonatomic, readonly, copy) NSString *longDesc;
@property (nonatomic, readonly, assign) BOOL canTransact;

@end

#endif /* TMPlanInfo_h */
