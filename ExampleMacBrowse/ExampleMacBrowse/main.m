//
//  main.m
//  ExampleMacBrowse
//
//  Created by Chandrashekhar Shetty on 11/18/15.
//  Copyright © 2015 Tinkerix. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <TalkMoreSDK/TalkMoreSDK.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        // insert code here...
        NSString *apiKey = [NSString stringWithUTF8String:argv[1]];
        TalkMore *talkMore = [[TalkMore alloc] initWithApiKey:apiKey];
        
        /*
        NSLog(@"Issuing browse command for Vodafone/Mumbai/GSM");
        [talkMore browsePlans:VODAFONE forMobileServiceType:GSM forCircle:MUM completion:^(TMPlans *plans, NSError *error) {
            if (!error) {
                NSLog(@"Got Plans %@", plans);
                for (int i = FULL_TALKTIME; i <= NIGHT; i++) {
                    if ([plans hasPlansForTag:i]) {
                        NSLog(@"Got %lu plan(s) for %@", (unsigned long)[[plans getPlansForTag:i] count], NSStringFromTMPlanTag(i));
                    } else {
                        NSLog(@"No plans for %@", NSStringFromTMPlanTag(i));
                    }
                }
            } else {
                NSLog(@"Got Error %@", error);
            }
            CFRunLoopStop([[NSRunLoop mainRunLoop] getCFRunLoop]);
        }];
         */
        
        NSLog(@"Issuing recharge command for Vodafone/Mumbai/GSM");
        [talkMore doRecharge:@"144992516584491" forMobile:@"9876543210" forEmail:@"chandra@tinkerix.com" forProviderType:VODAFONE forMobileServiceType:GSM forCircle:MUM forPostPaid:NO forAmount:10 forRecharges:@"10" forDeviceId:NULL forInstallId:NULL forSpecialRecharge:NO completion:^(TMOrder *order, NSError *error) {
            if (!error) {
                NSLog(@"Order %@", order);
            } else {
                NSLog(@"Got Error %@", error.localizedDescription);
            }
            CFRunLoopStop([[NSRunLoop mainRunLoop] getCFRunLoop]);
        }];
        
        
        CFRunLoopRun();
        NSLog(@"End of Run");
    }
    return 0;
}
