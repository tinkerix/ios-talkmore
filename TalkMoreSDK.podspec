#
# Be sure to run `pod lib lint foo.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
s.name             = "TalkMoreSDK"
s.version          = "0.1.7"
s.summary          = "TalkMore SDK"

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!
s.description      = <<-DESC
                     The TalkMore SDK for iOS and OS X allows you to use the TalkMore Platform to:
                     - Browse Plans
                     - Get plan recommendations
                     - Do recharges
                     DESC

s.homepage         = "https://talkmoreapp.com"
s.license          = 'MIT'
s.author           = { "TalkMore" => "support@talkmoreapp.com" }
s.source           = { :git => "https://bitbucket.org/tinkerix/ios-talkmore.git", :tag => s.version.to_s }
s.platform     = :ios, '7.0'
s.requires_arc = true
s.source_files = 'TalkMoreSDK/*.{h,m}'
s.dependency 'Mantle', '~> 2.0.5'
end
